# GLR Service Account for Olympus Account

resource "aws_iam_role" "gitlab_runner_service_account" {
  count              = var.environment == "olympus" ? 1 : 0
  name               = "GitlabRunnerExcecutor"
  assume_role_policy = data.aws_iam_policy_document.gitlab_runner_service_account[count.index].json
}

data "aws_iam_policy_document" "gitlab_runner_service_account" {
  count = var.environment == "olympus" ? 1 : 0
  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]
    effect  = "Allow"

    condition {
      test     = "StringEquals"
      variable = "${data.aws_ssm_parameter.eks_oidc_token_services[count.index].value}:aud"
      values   = ["sts.amazonaws.com"]
    }

    principals {
      identifiers = ["arn:aws:iam::${data.aws_caller_identity.current.id}:oidc-provider/${data.aws_ssm_parameter.eks_oidc_token_services[count.index].value}"]
      type        = "Federated"
    }
  }
}

# GLR Service Account Policy for Olympus Account to assume GitLab Job Role

data "template_file" "gitlab_runner_service_account_assume_role" {
  count    = var.environment == "olympus" ? 1 : 0
  template = data.aws_iam_policy_document.gitlab_runner_service_account_assume_role.json
}

data "aws_iam_policy_document" "gitlab_runner_service_account_assume_role" {
  statement {
    actions = ["sts:AssumeRole"]
    resources = ["arn:aws:iam::${var.excecutor_account_id}:role/GitlabRunnerJobRole"]
  }

  dynamic "statement" {
    for_each = var.job_runnner_account_ids
    content {
    actions = ["sts:AssumeRole"]
    resources = ["arn:aws:iam::${statement.value}:role/GitlabRunnerJobRole"]   
     }
  }
}


resource "aws_iam_policy" "gitlab_runner_service_account_assume_role" {
  count       = var.environment == "olympus" ? 1 : 0
  name        = "GitlabRunnerExcecutorCrossAccountPolicy"
  description = "Allow full Access for GitlabRunnerexcecutor Role"
  policy      = data.template_file.gitlab_runner_service_account_assume_role[count.index].rendered
}

resource "aws_iam_role_policy_attachment" "gitlab_runner_service_account_assume_role" {
  count      = var.environment == "olympus" ? 1 : 0
  role       = aws_iam_role.gitlab_runner_service_account[count.index].name
  policy_arn = aws_iam_policy.gitlab_runner_service_account_assume_role[count.index].arn
}

#GitLab Job Role That excecutor can assume

data "template_file" "gitlab_Job_trust_policy" {
  template = file("${path.module}/policies/gitlab-runner/gitlab-executor-trust-policy.json")
  vars = {
    excecutor_account_id = var.excecutor_account_id
  }
}

resource "aws_iam_role" "gitlab-Job" {
  name                  = "GitlabRunnerJobRole"
  force_detach_policies = true
  max_session_duration  = 43200
  assume_role_policy    = data.template_file.gitlab_Job_trust_policy.rendered
}

data "template_file" "gitlab_runner_Job_permissions" {
  template = file("${path.module}/policies/gitlab-runner/gitlab-job-policy.json")
}

resource "aws_iam_policy" "gitlab_runner_Job_permissions" {
  name        = "GitlabRunnerTerraformPermissionsPolicy"
  description = "Allow full Access for Gitlab Runner Role expect IAM"
  policy      = data.template_file.gitlab_runner_Job_permissions.rendered
}

resource "aws_iam_role_policy_attachment" "gitlab_runner_Job" {
  role       = aws_iam_role.gitlab-Job.name
  policy_arn = aws_iam_policy.gitlab_runner_Job_permissions.arn
}
