#OIDC TOKENS
data "aws_ssm_parameter" "eks_oidc_token_services" {
  count = var.environment == "olympus" ? 1 : 0
  name  = "OLYMPUS_OIDC_PROVIDER"
}

data "aws_caller_identity" "current" {}

#GLR Group token
data "aws_ssm_parameter" "glr_group_token" {
  count = var.environment == "olympus" ? 1 : 0
  name  = "OLYMPUS_GROUP_GLR_TOKEN"
}