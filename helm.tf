resource "helm_release" "gitlab_runner" {
  count            = var.environment == "olympus" ? 1 : 0
  name             = local.runner_name
  namespace        = local.runner_name
  create_namespace = true
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-runner"
  version          = "0.38.1"

  values = [
    "${file("${path.module}/values.yaml")}"
  ]

  set {
    name  = "runnerRegistrationToken"
    value = data.aws_ssm_parameter.glr_group_token[count.index].value
  }

  set {
    name  = "gitlabUrl"
    value = "https://gitlab.com/"
  }

  set {
    name  = "fullnameOverride"
    value = local.runner_name
  }

  set {
    name  = "rbac.serviceAccountAnnotations.eks\\.amazonaws\\.com/role-arn"
    value = aws_iam_role.gitlab_runner_service_account[count.index].arn
  }

  set {
    name = "rbac.podSecurityPolicy.resourceNames"
    value = local.runner_name
  }

  set {
    name = "runners.serviceAccountName"
    value = local.runner_name
  }

  set {
    name = "rbac.serviceAccountName"
    value = local.runner_name
  }

 depends_on = [ aws_iam_role.gitlab_runner_service_account ]

}
