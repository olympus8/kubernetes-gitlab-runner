terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>4.6"
    }
  }
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

provider "aws" {
  region = "eu-west-2"

  default_tags {
    tags = {
      Environment = var.environment
      Terraform   = basename(abspath(path.module))
      Project     = "terraform-gitlab-runner"
    }
  }
}

